import React from 'react' ;
import {StyleSheet, View, Button, TextInput, FlatList, Text, ActivityIndicator } from 'react-native' ;
import films from "../helpers/filmsData" ;
import FilmItem from './filmItem' ;
import {getFilmsFromApiWithSearchedText} from '../API/tmdbApi' ;
import Loading from './loading';

class Search extends React.Component {
    
    constructor( props ){
        super( props ) ;
        this.state = { 
            films : [],
            isLoading : false,
        } ;
        this.search_text = ( new Date().getFullYear() - 1 ).toString() ;
        this.page = 0 ;
        this.total_pages = 0 ;
    }

    _makeId(length) {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( var i = 0; i < length; i++ )
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        return result;
     }

    _call_getFilmsFromApiWithSearchedText() {
        getFilmsFromApiWithSearchedText( this.search_text, this.page + 1  )
            .then( data => {
                this.page = data.page ;
                this.total_pages = data.total_pages;
                this.setState({ 
                    films : [ ...this.state.films, ...data.results ],
                    isLoading : false,
                }) ;
            })
    }

    _loadFilms() {
        this.setState({ isLoading : true }) ;
        if( this.search_text == '' ){
            this.search_text = ( new Date().getFullYear() - 1 ).toString() ;
            this._call_getFilmsFromApiWithSearchedText() ;
        }

        else{
            this._call_getFilmsFromApiWithSearchedText() ;
        }
    }

    _searchFilms() {
        this.setState({
            films : [],
        }, () => {
            this.page = 0 ;
            this.total_pages = 0 ;
            this._loadFilms() ;
        });
    }

    _displayLoading() {
        if( this.state.isLoading == true ){
            return(
               <Loading top={100} />
            )
        }
    }

    _displayDetailsForFilm = ( idFilm ) => {
        //console.log(  this.props.navigation ) ;
        this.props.navigation.navigate( 
            'Details' , 
            {
                idFilm : idFilm
            }
        ) ;
    }

    componentDidMount() {
        this._loadFilms() ;
    }

    render() {
        //this._loadFilms() ;
        //console.log( this.state.isLoading ) ;
        return (
            <View style={style.view}>
                <View>
                    <TextInput
                        style={style.text_input}
                        placeholder="Film title"
                        onChangeText={ (text) => this.search_text = text }
                        onSubmitEditing={ () => {
                            this._searchFilms() ;
                        }}
                    />

                    <Button
                        color="#fd7812"
                        title="Search"
                        onPress={ () => {
                            this._searchFilms() ;
                        }}
                        style={style.button_}
                    />

                    <View style={style.aft}></View>
                </View>

                <FlatList
                    data={ this.state.films }
                    keyExtractor={ (item) => item.id.toString() + this._makeId(8) }
                    renderItem={({ item }) => {
                        return(
                            <FilmItem
                                film={item}
                                displayDetailsForFilm={ this._displayDetailsForFilm }
                            />
                        )
                    }}
                    style={style.flat_list}
                    onEndReachedThreshold={ 0.5 }
                    onEndReached={ () => {
                        if( this.page < this.total_pages ){
                            this._loadFilms() ;
                        }
                    }}
                />
                { this._displayLoading() }
            </View>
        );
    }
}

const style = StyleSheet.create({
    view : {
        marginTop : 0,
        flex : 1,
        backgroundColor : '#fff',
        borderColor : "#fd7812",
        borderTopWidth : 0 ,
        borderLeftWidth : 0 ,
        borderRightWidth : 0 ,
        borderBottomWidth : 0,
    },

    text_input : {
        marginLeft : 0,
        marginRight : 0,
        height : 60,
        borderColor : "#fd7812",
        borderTopWidth : 0 ,
        borderLeftWidth : 0 ,
        borderRightWidth : 0 ,
        borderBottomWidth : 5,
        paddingLeft : 5,
        paddingRight : 5,
    },

    flat_list : {
        
    },

    button_ : {
        height : 90,
    },

    aft : {
        borderColor : "#fd7812",
        borderTopWidth : 0 ,
        borderLeftWidth : 0 ,
        borderRightWidth : 0 ,
        borderBottomWidth : 5,
    }
});

export default Search