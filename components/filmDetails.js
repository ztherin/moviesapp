// Components/FilmDetail.js

import React from 'react' ;
import { StyleSheet, View, Text, ScrollView, Image } from 'react-native' ;
import Loading from './loading';
import {getFilmDetails} from '../API/tmdbApi' ;
import moment from "moment" ;
import numeral from "numeral" ;

class FilmDetail extends React.Component {
  constructor( props ) {
    super( props ) ;
    this.state = { 
      film : undefined,
      isLoading : true,
    } ;
    this.FILM_IMAGE_LINK = 'https://image.tmdb.org/t/p/w300' ;
  }

  componentDidMount() {
    getFilmDetails( this.props.route.params.idFilm )
      .then( data => {
        this.setState({ 
            film : data,
            isLoading : false,
        }) ;
      }) ;
  }

  _displayFilm() {
    if( this.state.film != undefined ) {
      return(
        <ScrollView style={ styles.viewStyle }>
          <View style={styles.imageContainer}>
            <Image
              style={ styles.detailsImage }
              source={{ uri : this.FILM_IMAGE_LINK + this.state.film.backdrop_path }}
            />
          </View>
          
          <View style={styles.titleContainer}>
            <Text style={ styles.title }>
              { this.state.film.original_title + '\n\n'}
              <Text>{ this.state.film.title }</Text>
            </Text>
          </View>

          <View style={ styles.overviewContainer }>
            <Text style={ styles.overview }>
              { this.state.film.overview }
            </Text>
          </View>

          <View style={ styles.bottomInfo }>
              <Text style={styles.bottomheader}>
                Released on { moment( this.state.film.release_date ).add(0,'day').format('LL') + "\n" } ( {moment( this.state.film.release_date ).fromNow()} )
              </Text>
              
              <View style={styles.info}>
                <Text style={ styles.label }>Note :</Text>
                <Text style={styles.textValue}>{this.state.film.vote_average} / 10</Text>
              </View>
              
              <View style={styles.info}>
                <Text style={ styles.label }>Number of votes :</Text>
                <Text style={styles.textValue}>{ this.state.film.vote_count }</Text>
              </View>
              
              <View style={styles.info}>
                <Text style={ styles.label }>Budget : </Text>
                <Text style={styles.textValue}>{ numeral( this.state.film.budget ).format('0,0 $')}</Text>
              </View>
              
              <View style={styles.info}>
                <Text style={ styles.label }>Genre :</Text>
                <Text style={styles.textValue}>{
                  this.state.film.genres.map( function( genre ){
                    return genre.name ;
                  }).join( " / " ) }
                </Text>
              </View>
              
              <View style={styles.info}>
                <Text style={ styles.label }>Companie(s) :</Text>
                <Text style={styles.textValue}>{
                  this.state.film.production_companies.map(function(company){
                    return company.name;
                  }).join(" / ")}
                </Text>
              </View>
            
            </View>

        </ScrollView>
      )
    }
  }

  _displayLoading() {
    if( this.state.isLoading == true ){
        return(
           <Loading top={0}/>
        )
    }
  }
  render() {
    const idFilm = this.props.route.params.idFilm ;
    return (
      <View style={styles.main_container}>
        { this._displayFilm() }
        { this._displayLoading() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
  },

  viewStyle: {
    flex : 1,
    top : 0 ,
    height : '100%',
    width : '100%',
    position : 'absolute',
    borderWidth : 0,
    borderColor : '#000',
  },

  imageContainer: {
    height : 200,
    width : '100%',
    backgroundColor : '#999'
  },

  detailsImage: {
    width : '100%',
    height : '100%',
  },

  titleContainer: {
    padding : 10,
    alignItems : 'center'
  },

  title: {
    textAlign : 'center',
    fontWeight : 'bold',
    fontSize : 25,
    color : '#fd7812',
  },

  overviewContainer: {
    padding : 10,
  },

  overview: {
    lineHeight : 24,
    fontSize : 18,
    color : '#999',
  },

  bottomInfo: {
    padding : 10,
  },

  bottomheader: {
    textAlign : 'center',
    color : '#fd7812',
    fontWeight : 'bold',
    fontSize : 18,
  },

  info : {
    flexDirection : 'row',
    marginTop : 15,
  },

  label: {
    flex : 1,
    fontSize : 17,
    color : '#555',
  },

  textValue : {
    flex : 1,
    color : '#fd7812',
    fontWeight : 'bold',
    fontSize : 18,
  }
})

export default FilmDetail ;