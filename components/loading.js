import React from 'react' ;
import {StyleSheet, View, Image } from 'react-native' ;

class Loading extends React.Component {
    constructor( props ) {
        super( props ) ;
    }

    render() {
        const top = this.props.top ;
        return (
            <View style={[style.loading_container, {top : this.props.top}]}>
                <Image
                    style={style.loading_image} 
                    source={ require('../assets/images/Comp-2.gif') }
                />
            </View>
        );
    }
}

const style = StyleSheet.create({
    loading_container : {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        flex : 1,
        opacity : 0.5,
    },
    loading_image : {
        flex : 1,
        opacity : 1,
    }
});

export default Loading ;