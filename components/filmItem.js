import React from 'react' ;
import {StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native' ;
import "../react-native.config" ;

class FilmItem extends React.Component {
    constructor( props ) {
        super( props ) ;
        this.FILM_IMAGE_LINK = 'https://image.tmdb.org/t/p/w300' ;
    }
    render() {
        //const film = this.props.film ;
        //const displayDetailsForFilm = this.props.displayDetailsForFilm ;

        const{ film, displayDetailsForFilm } = this.props ;
        return (
            <TouchableOpacity 
                style={styles.main_container}
                onPress={ () => displayDetailsForFilm( film.id ) }
            >
                <Image
                    style={styles.image_style} 
                    source={{ uri : this.FILM_IMAGE_LINK + film.poster_path }}
                />
                <View style={styles.content_container}>
                    <View style={styles.header_container}>
                        <Text style={styles.title_text}>{film.title}</Text>
                        <Text style={styles.vote_text}>{film.vote_average}</Text>
                    </View>

                    <View style={styles.description_container}>
                        <Text style={styles.description_text} numberOfLines={6}>{film.overview}</Text>
                    </View>

                    <View style={styles.date_container}>
                        <Text style={styles.date_text}>Sortie le {film.release_date}</Text>
                    </View>

                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    main_container : {
        height : 190,
        flexDirection : 'row',
        marginTop : 10,
        borderColor : "silver",
        marginLeft : 5,
        marginRight : 5,
        borderRadius : 5,
        shadowColor : '#000000aa',
        shadowOpacity : 1,
    },

    image_style : {
        width : 120,
        margin : 5,
        backgroundColor : 'gray',
    },

    content_container : {
        flex : 1,
        margin : 5,
    },

    header_container : {
        flex : 3,
        flexDirection : 'row',
    },

    title_text : {
        fontWeight : 'bold',
        fontSize : 20,
        flex : 1,
        flexWrap : 'wrap',
        paddingRight : 5,
    },

    vote_text : {
        fontWeight : 'bold',
        fontSize : 26,
        color : '#666666',
    },

    description_container : {
        flex : 7,
    },

    description_text : {
        fontStyle : 'italic',
        color : '#666666',
    },

    date_container : {
        flex : 1,
    },

    date_text : {
        textAlign : 'right',
        fontSize : 14,
    },
});

export default FilmItem
