import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Search from "./components/search" ;
import FilmDetail from "./components/filmDetails" ;

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Search" 
          component={Search}
        />

        <Stack.Screen
          name="Details" 
          component={FilmDetail}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;