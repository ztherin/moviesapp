import API_TOKEN from "./apiToken" ;

export function getFilmsFromApiWithSearchedText( text, page ) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text + '&page=' + page
    return fetch( url )
        .then(( response ) => response.json())
        .catch(( error ) => console.log( error ))
}

export function getFilmDetails( id ){
    const url = 'https://api.themoviedb.org/3/movie/' + id + '?api_key=' + API_TOKEN + '&language=fr'
    return fetch( url )
        .then((response) => response.json())
        .catch((error) => console.error(error));
}